/**********************************************************************
 *
 * Triangulation
 *
 * Triangulation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * Triangulation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Triangulation.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************
 *
 * Copyright (C) 2019 Nicklas Avén
 *
 ***********************************************************************/


#include "../src/triangulation.h"
#include "postgres.h"
#include "fmgr.h"
#include "liblwgeom.h"
#include "catalog/pg_type.h"
#include "utils/array.h"
#include "utils/lsyscache.h" /* for get_typlenbyvalalign */

#define PG_GETARG_GSERIALIZED_P(varno) ((GSERIALIZED *)PG_DETOAST_DATUM(PG_GETARG_DATUM(varno)))


#define __PG__

/*
 * This is required for builds against pgsql
 */
PG_MODULE_MAGIC;

/**
* POLYGON
*/
static LWPOLY* build_tri(TRI *t,int srid)
{
	uint32_t i;
	LWPOLY *poly;

	/* Start w/ empty polygon */
	poly = lwpoly_construct_empty(srid, 0, 0);


	POINTARRAY *pa = NULL;

	pa = ptarray_construct(0,0,4);

	double *dlist = (double*)(pa->serialized_pointlist);
    

          POINT_2D *p1 = t->edges[0]->p[t->flipped[0]];
            POINT_2D *p2 = t->edges[1]->p[t->flipped[1]];
            POINT_2D *p3 = t->edges[2]->p[t->flipped[2]];
            
            dlist[0] = p1->x;
            dlist[1] = p1->y;
            dlist[2] = p2->x;
            dlist[3] = p2->y;
            dlist[4] = p3->x;
            dlist[5] = p3->y;
            dlist[6] = p1->x;
            dlist[7] = p1->y;
            
		

		/* Add ring to polygon */
		lwpoly_add_ring(poly, pa);
		

	
	return poly;
}

static int remove_super_tris(TRI_LIST *t_list)
{
        int s;
        POINTLIST *plist = t_list->p_list;
        for (s = 1;s<=3;s++)
        {
            POINT_2D *p = plist->points - s;
            int r;
            for (r=0;r<p->n_used_in;r++)
            {
                EDGE *e = p->used_in[r];
                if(e->tri[0])
                    e->tri[0]->status = unused;
                if(e->tri[1])
                    e->tri[1]->status = unused;
            }
        }
        return 0;
}
/**
* MULTIPOLYGON
*/
static LWGEOM* construct_tri_collection(TRI_LIST **tri_lists, int n, int constrained, int srid)
{
	int ngeoms, i, j;
	LWGEOM *geom = NULL;
	LWCOLLECTION *col = lwcollection_construct_empty(MULTIPOLYGONTYPE, srid, 0,0);

    //Mark triangels used by super triangle unused
    if(!constrained)
    {
        remove_super_tris(tri_lists[0]);
    } 
    
    for (j=0;j<n;j++)
    {
        
        TRI_LIST *t_list = tri_lists[j];
        


        /* Read number of geometries */
        ngeoms = t_list->tri_list_used;

        for ( i = 0; i < ngeoms; i++ )
        {
            TRI *t = t_list->tri_list + i;
            if(t->status == used)
            {
                geom = lwpoly_as_lwgeom(build_tri(t, srid));
                if ( lwcollection_add_lwgeom(col, geom) == NULL )
                {
                    printf("Unable to add geometry (%p) to collection (%p)", geom, col);
                    return NULL;
                }
            }
                
        }
    }
	return (LWGEOM*) col;
}

static int get_tri_indexes(TRI_LIST **tri_lists, int n, int constrained, Datum **res)
{
    int i,j;
    //Mark triangels used by super triangle unused
    if(!constrained)
    {
        remove_super_tris(tri_lists[0]);
    } 
    int tot_number_of_triplets = 0;
    for (j=0;j<n;j++)
    {
        tot_number_of_triplets += tri_lists[j]->tri_list_used;
    }  
    uint32_t counter = 0;
    uint32_t used_points = 0;
    POINT_2D *start = tri_lists[0]->p_list->points;
    Datum *res_tmp = palloc(tot_number_of_triplets * 3 * sizeof(Datum));
    for (j=0;j<n;j++)
    {        
        TRI_LIST *t_list = tri_lists[j];
 
        /* Read number of geometries */
        int n_tri = t_list->tri_list_used;

        for ( i = 0; i < n_tri; i++ )
        {
            TRI *t = t_list->tri_list + i;
            if(t->status == used)
            {
                used_points++;
                POINT_2D *p1 = t->edges[0]->p[t->flipped[0]];
                POINT_2D *p2 = t->edges[1]->p[t->flipped[1]];
                POINT_2D *p3 = t->edges[2]->p[t->flipped[2]];
                res_tmp[counter++] = UInt32GetDatum(p1 - start);
                res_tmp[counter++] = UInt32GetDatum(p2 - start);
                res_tmp[counter++] = UInt32GetDatum(p3 - start);
            }
                
        }
    }
    elog(NOTICE, "counter = %d used_points = %d\n", counter, used_points);
    *res = res_tmp;
    return used_points;
    
}
static TRI_LIST* tri_polygon(const LWPOLY *poly, int constrained)
{       
        uint32_t n_points = 0;
        int i;
        for (i = 0;i<poly->nrings;i++)
        {
            
            n_points += poly->rings[i]->npoints;
        }
        
        POINTLIST *plist = pointlist_init(n_points);
        
        
        for (i = 0;i<poly->nrings;i++)
        {
            POINTARRAY *pa = poly->rings[i];
            
            plist_add_ring(plist,(double*) pa->serialized_pointlist ,pa->npoints);
        }
                
        
        return triangulate_2_t_list(plist, constrained);
}

static TRI_LIST**  tri_multi_polygon(const LWCOLLECTION *col, int constrained)
{
    uint32_t i,j;

    TRI_LIST **tri_lists = malloc(col->ngeoms * sizeof(TRI_LIST*));
	for ( i = 0; i < col->ngeoms; i++ )
	{
		tri_lists[i] = tri_polygon((LWPOLY*) col->geoms[i], constrained);
        if(!tri_lists[i])
        {
            for (j=0;j<i;j++)
            {
                TRI_LIST *t_list = tri_lists[j];
                cleanup(t_list);
            }
            return NULL;
        }

	}
	return tri_lists;
}

/**
* Count rings in an #LWGEOM.
*/
static LWGEOM* tri(const LWGEOM *lwgeom,int constrained)
{
    int srid = lwgeom->srid;
    int lwtype = lwgeom->type;    
    if (lwtype == POLYGONTYPE)
    {
        LWPOLY *poly = (LWPOLY*) lwgeom;
        TRI_LIST *t_list = tri_polygon(poly, constrained);
        if(!t_list)
        {
            cleanup(t_list);
            return NULL;
        }
        LWGEOM *res = construct_tri_collection(&t_list, 1, constrained, srid);
        cleanup(t_list);
        return res;
    }
    else if(lwtype == MULTIPOLYGONTYPE)
    {
        LWCOLLECTION *col = (LWCOLLECTION*) lwgeom;
        TRI_LIST **tri_lists = tri_multi_polygon(col, constrained);
        if(!tri_lists)
            return NULL;
        LWGEOM *res = construct_tri_collection(tri_lists, col->ngeoms, constrained, srid);
        
        int i;
        for (i=0;i<col->ngeoms;i++)
        {
         TRI_LIST *t_list = tri_lists[i];
         cleanup(t_list);
        }
        return res;
    }
    else
    {
        elog(NOTICE, "Not a polygon or multipolygon\n");
        return NULL;
    }
}


static int tri_index(const LWGEOM *lwgeom,int constrained, Datum **res)
{
    int lwtype = lwgeom->type;    
    if (lwtype == POLYGONTYPE)
    {
        LWPOLY *poly = (LWPOLY*) lwgeom;
        TRI_LIST *t_list = tri_polygon(poly, constrained);
        int n_triplets = get_tri_indexes(&t_list, 1, constrained, res);
        cleanup(t_list);
        return n_triplets;
    }
    else if(lwtype == MULTIPOLYGONTYPE)
    {
        LWCOLLECTION *col = (LWCOLLECTION*) lwgeom;
        TRI_LIST **tri_lists = tri_multi_polygon(col, constrained);        
        int n_triplets = get_tri_indexes(tri_lists, col->ngeoms, constrained, res);
        
        int i;
        for (i=0;i<col->ngeoms;i++)
        {
         TRI_LIST *t_list = tri_lists[i];
         cleanup(t_list);
        }
        return n_triplets;
    }
    else
    {
        elog(NOTICE, "Not a polygon or multipolygon\n");
        return 1;
    }
}




Datum pg_tri(PG_FUNCTION_ARGS);
PG_FUNCTION_INFO_V1(pg_tri);
Datum pg_tri(PG_FUNCTION_ARGS) 
{
    GSERIALIZED *outgeom;
    
    GSERIALIZED *geom = PG_GETARG_GSERIALIZED_P(0);

	int constrained = PG_GETARG_INT32(1);
    
	LWGEOM *lwgeom = lwgeom_from_gserialized(geom);
    LWGEOM *res = tri(lwgeom, constrained);
    if(res)
    {
        outgeom = gserialized_from_lwgeom(res, NULL);       
        
        PG_FREE_IF_COPY(geom, 0);
        PG_RETURN_POINTER(outgeom);
    }
    else
        PG_RETURN_NULL();
}


Datum pg_tri2index(PG_FUNCTION_ARGS);
PG_FUNCTION_INFO_V1(pg_tri2index);
Datum pg_tri2index(PG_FUNCTION_ARGS)
{
    
    GSERIALIZED *geom = PG_GETARG_GSERIALIZED_P(0);

	int constrained = PG_GETARG_INT32(1);
    
	LWGEOM *lwgeom = lwgeom_from_gserialized(geom);

    
    Oid         element_type = INT4OID;

    int16       typlen;
    bool        typbyval;
    char        typalign;
    int         ndims;
    int         dims[2];
    int         lbs[2];



    Datum *res = NULL;
    int size = tri_index(lwgeom,constrained, &res);

    ndims = 2;
    dims[0] = size;
    dims[1] = 3;
    lbs[0] = 0;
    lbs[1] = 0;
    
        
    get_typlenbyvalalign(element_type, &typlen, &typbyval, &typalign);

 
    ArrayType  *result = construct_md_array(res, NULL, ndims, dims, lbs,element_type, typlen, typbyval, typalign);
    
    if(res)
        PG_RETURN_POINTER(result);
    else        
        PG_RETURN_NULL();
}
