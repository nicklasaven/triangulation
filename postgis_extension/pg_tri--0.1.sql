-- complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION pg_twkb" to load this file. \quit





CREATE OR REPLACE FUNCTION triangulate(geom geometry, constrained int default 1) RETURNS geometry
  AS 'MODULE_PATHNAME', 'pg_tri'
  LANGUAGE C IMMUTABLE;
  
CREATE OR REPLACE FUNCTION triangulate2index(geom geometry, constrained int default 1) RETURNS int[]
  AS 'MODULE_PATHNAME', 'pg_tri2index'
  LANGUAGE C IMMUTABLE;
