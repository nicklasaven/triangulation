CC=gcc
CFLAGS="-Wall"

debug:clean
	$(CC) $(CFLAGS) -g -o triangulation main.c src/triangulation.c
stable:clean
	$(CC) $(CFLAGS) -o triangulation main.c src/triangulation.c
clean:
	rm -vfr *~ triangulation
