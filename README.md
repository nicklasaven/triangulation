**Triangulation**

This is a program for creating a constrained or unconstrained delaunay triangulation.

It is far from ready and when it crashes, it *crashes hard!*

*You are warned!*


It is built from the algorithm presented here:
https://www.newcastle.edu.au/__data/assets/pdf_file/0019/22519/23_A-fast-algortithm-for-generating-constrained-Delaunay-triangulations.pdf

It seems pretty fast.

As is, it is for handling PostGIS geometries, but the result is prepared independant of PostGIS so it can be used in other applications too.

It can handle touching rings and invalid geoemtries with boundary touching itself at 1 point. 
But there are several cases of invalid geometries that is not handled. Some of the should etix with error, and some will just blow the program (I suspect)

*To run the code as a PostGIS extension*
* cd to the directory postgis_extension
* edit the Make file in that folder to suite your system and PostgreSQL version
* make
* make install
* In the database CREATE EXTENSION pg_tri;

To run triangulation in PostGIS (it handles polygons and multipolygons)

For constrained triangulation
SELECT triangulate(geom) from table;

For unconstrained:
SELECT triangulate(geom, 0) from table;


The code is uner GPL 3 license