/**********************************************************************
 *
 * Triangulation
 *
 * Triangulation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * Triangulation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Triangulation.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************
 *
 * Copyright (C) 2019 Nicklas Avén
 *
 ***********************************************************************/

#ifndef _triangulation_H_
#define _triangulation_H_


#include <stdlib.h>
#include <stdint.h>

#undef __PG__

#define PRE_ALLOCED_USED_IN 6


typedef struct EDGE EDGE;
typedef struct POINTLIST POINTLIST;
typedef struct TRI TRI;
typedef struct TRI_LIST TRI_LIST;
typedef struct EDGE_LIST EDGE_LIST;
typedef struct POINT_2D POINT_2D;

/*In an edge point 0 is always a point before point 1 in the boundary
 * We define all triangles as counter clockwise
 * one of the triangles using an edge will use the points "as is" (unflipped),
 * and the other will use them "flipped" to construct the triangle
 So, this enum is used in the tri structure to tell how each edge is used.*/
typedef enum pointorder pointorder;
enum pointorder{unflipped, flipped};

typedef enum boolean boolean;
enum boolean{false, true};


/*This is used for the final state of the triangles
 * If creating a constrained triangulation all tri inside the polygon 
 * should be "used"
 * If unconstrained all triangles not used by the super triangle 
 * should be "used"*/
typedef enum tri_status tri_status;
enum tri_status{used, unused}; //This way default is used since we calloc the memory. This is important since we don't evaluate boundary triangles, or we might miss triangles from 3 point input. triangle holes is a special case, handled below.

/*This is used for regiestering the status of each edge as the process moves on. 
 * Any triangle with an edge marked as outside when ready will cause the triangle to be marked unused*/
typedef enum edge_status edge_status;
enum edge_status{undefined, outside, crossing, inside, boundary};


/*Here we keep all the triangles. We allocate the maximum number of triangles before starting
 */
struct TRI_LIST
{
    TRI *tri_list;
    size_t tri_list_alloced;
    size_t tri_list_used;
    EDGE_LIST *e_list; // just a handler for cleanup
    POINTLIST *p_list; // just a handler for cleanup
};


/*All points from original input gets loaded in this
 * What it is used for:
        * x and y: quite obvious :-)
        * used_in: a list with edges that uses this point. At start there is 
          preallocated space for PRE_ALLOCED_USED_IN number of edges.
          If more is needed the list is moved to separate allocation
          This preallocated number should maybe be raised from 4 now.
          Some statistics should be collected
        * use_instead: if a point is shared between multiple rings or by a self intersection
          we use the first occation in the triangulation, so, when iterating the point list
          finding one of the later occations we use the first use_instead.
        * replacing: What point this points is replacing. Is not used now so we could 
          use "use_instead" for multiple points removing "replacing" without the need to handle that as a list
    */
struct POINT_2D
{
        double x;
        double y;
        EDGE* *used_in;
        size_t n_used_in;
        size_t alloced_used_in; //if point is used in more than PRE_ALLOCED_USED_IN edges, used_in is moved to allocated memory and has to be freed separetly
        uint8_t edgelist2b_freed;
        POINT_2D *use_instead;
        POINT_2D *replacing;
        
};


/*A structure holding the list of points. The "prealloced_used_in" is the anchor point 
 * for the preallocated "used_in" described for POINT_2D*/
struct POINTLIST
{
        POINT_2D *points;
        int n_rings;
        int tot_npoints;
        int *n_points_per_ring; //array describing how many points each ring has
        int *point_in_ring_val; //array describing how many points each ring has
        size_t n_points_per_ring_alloced;
        double maxXY[2];
        double minXY[2];
        size_t points_alloced;
        size_t points_used;
        EDGE* *prealloced_used_in;
} ;

/*A single triangle have 3 edges, and 3 corresponding registrations if 
 * the edge should be used "flipped or "unflipped"
 * */
struct TRI
{
        EDGE* edges[3];
        pointorder flipped[3];
        tri_status status;
};


/*An Edge is described with reference to the used points and how 
 * the edges is used in the triangles
    * p[2] is the 2 points. p[0] is always earlier in the point array than point[1]
    * tri[2]: the two triangles. tri[1] is always using the edge "unflipped"
      while the tri[1] always uses it "flipped"
    * edge_pos_in_tri: tells where in the tri[0] and tri[1] the edge is used
    * tri_status: isn't used, but was an intended to be used 
      instead og edge status to be able to handle 3 point holes (with only boundaries)
      But first attempt failed but is left for a while as a reminder :-)
*/
struct EDGE
{
    POINT_2D* p[2];
    TRI* tri[2]; //tri 1 uses the edge "as is" point1 -> point2 tri 2 uses it flipped
    uint8_t edge_pos_in_tri[2];
    edge_status status; //used to lock boundary edges when creating constrained delunay
    int tri_status[2];
} ;

/*Just a holder for the list of edges*/
struct EDGE_LIST
{
    EDGE *edges;
    size_t e_list_alloced;
    size_t e_list_used;
};


/*This has nothing to du with below TEMP_EDGES (should get a new name or use the above instead
 * don't remember now why it is a separate) 
 * It is just a list of edges to hold for temporary usage like intersecting edges, and newly created edges
 * waiting for dealynay restauration*/
typedef struct 
{
    EDGE* *edges;
    size_t alloced_edges;
    size_t used_edges;    
} TEMP_EDGES_LIST;

/*This is a special structure used between function calls to manipulate 
 * a single edge. This can probably be refined and maybe splitted.
 * Now all info used for different purposes is just thrown here*/
typedef struct 
{
    EDGE *edge;
    TRI *tri;
    POINT_2D *p;
    uint8_t edge_location; 
    uint8_t flipped;     
} TEMP_EDGE;


/* Functions used outside triangle.c. Should be moved to api-header (and marked external) toghether with 
 * needed structs above if this should be used as lib*/
int plist_add_ring(POINTLIST *plist, double *coordinates,int n_points);
POINTLIST* pointlist_init(int npoints);

int triangulate_2_text(POINTLIST *plist, int constrained);
TRI_LIST* triangulate_2_t_list(POINTLIST *plist, int constrained);
int cleanup(TRI_LIST *t_list);

void print_all_tri2file(TRI_LIST *t_list);

#endif


