/**********************************************************************
 *
 * Triangulation
 *
 * Triangulation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * Triangulation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Triangulation.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************
 *
 * Copyright (C) 2019 Nicklas Avén
 *
 ***********************************************************************/

#include "triangulation.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include <stdarg.h>


#ifdef __PG__
#include <postgres.h>
#endif


int layer_counter;
static int max_iterations;
int iteration_counter;
/*This is used for debugging, to be able to easily print all triangles from 
 * anywhere in the code*/
static TRI_LIST *global_tlist;


void print_all_tri2file(TRI_LIST *t_list);
void print_tri2file(TRI *t, int id, char *te);
void print_edge2file(EDGE *e, int id, char *te);
void print_point2file(POINT_2D *p, int id, char *te);


/**************************************************************************************
*******************     Definitions       ******************************************
***************************************************************************************/


static TEMP_EDGES_LIST *intersecting_edge_list;
static TEMP_EDGES_LIST *new_edges_list;

/*Just a way to get the next and previous number connecting 2 to 0 and 0 to 2*/
static int next_edge[3] = {1,2,0};
static int prev_edge[3] = {2,0,1};


/*A function to handle different ways to give output on differnet platforms and situations*/
static void log_this( const char *log_txt, ... )
{
    char log_txt_tot[8192];

    va_list args;
    va_start (args, log_txt);
    vsnprintf (log_txt_tot,8192,log_txt, args);
    va_end (args);

#ifdef __ANDROID__

    __android_log_print(ANDROID_LOG_INFO, APPNAME,"%s\n",log_txt_tot);
#endif
#ifdef __PG__
    elog(NOTICE,"%s\n", log_txt_tot);
#else

    printf("%s",log_txt_tot);

#endif

    return;
}





/**************************************************************************************
*******************     UTILS       ******************************************
***************************************************************************************/

static inline double dot_product(POINT_2D *p1, POINT_2D *p2)
{
    return p1->x * p2->x + p1->y * p2->y;    
}



static inline int cross_product_norm_z(POINT_2D *va, POINT_2D *vb, POINT_2D *p)
{
    double res;
    POINT_2D p1, p2;
    
    p1.x = vb->x - va->x;
    p1.y = vb->y - va->y;
    p2.x = p->x - va->x;
    p2.y = p->y - va->y;
        
    res = p1.x * p2.y - p1.y * p2.x; 
    if(res == 0)
    {
        if(p2.x == 0 && p2.y == 0)
        {
            log_this("we have 2 identical points %lf, %lf\n", p->x, p->y);
            p->use_instead = va;
            va->replacing = p;
            return 0;
        }
        if(vb->x == p->x && vb->y == p->y)
        {
            log_this("we have 2 identical points %lf, %lf\n", p->x, p->y);
            p->use_instead = vb;
            vb->replacing = p;
            return 0;
        }
    }
    if(res > 0)
        return 1;
    else if (res < 0)
        return -1;
    else
        return 0;
}

/*If point is in triangel or on border return 0
 * Find first side that the point is outside and return it.
 * later we search in the adjecent triangle in that direction
 * 1 means outside v(p0 -> p1)
 * 2 means outside v(p1 -> p2)
 * 3 means outside v(p2 -> p0)
 */
static inline int point_in_triangle(TRI *t, POINT_2D *p)
{
    POINT_2D *p1 = t->edges[0]->p[t->flipped[0]]; //get first point from first edge in tri
    POINT_2D *p2 = t->edges[1]->p[t->flipped[1]]; //get first point from second edge in tri
    POINT_2D *p3 = t->edges[2]->p[t->flipped[2]]; //get first point from third edge in tri  
    
    int p_side_v1 = cross_product_norm_z(p1, p2, p);
    if(p->use_instead)
        return 0;
    if(p_side_v1<0)
        return 1;
    
    int p_side_v2 = cross_product_norm_z(p2, p3, p);
    if(p->use_instead)
        return 0;
    if(p_side_v2<0)
        return 2;
    
    int p_side_v3 = cross_product_norm_z(p3, p1, p);
    if(p->use_instead)
        return 0;
    if(p_side_v3<0)
        return 3;
    
    /*It should only be possible to get here if maximum one of the above return 0, since we check in 
     * cross_product_norm_z if the point we check is one of the points in the incomming triangle
     * If one of the above returns 0 it means that the point is on one of the borders. 
     * We handle that as inside and things should work
     * if we should handle that as outside it will not be fetched by any triangle
     * and we would go into an infinite loop (been there done that) */
    
    return 0;        
}

/*This is the test to see if one if the alone points of two triangles sharing an edge
 * is in the circumcircle of the other. If so we must switch the diagonal to respect the Delaunay triangulation
 * It is two versions here, described in the paper that this is built from.
 * one faster and one more robust. We have to find out which one to use
 * One of them is commented away.*/
static inline boolean swap_test(TRI *t, POINT_2D *p,int common_edge_pos)
{
    int next_edge[] = {1,2,0};
    
    POINT_2D *p1 = t->edges[common_edge_pos]->p[t->flipped[common_edge_pos]]; //get first point from first edge in tri
    POINT_2D *p2 = t->edges[common_edge_pos]->p[!t->flipped[common_edge_pos]]; //get first point from second edge in tri
    int n = next_edge[common_edge_pos];
    POINT_2D *p3 = t->edges[n]->p[!t->flipped[n]]; //get first point from third edge in tri  
    
    double x13 = p1->x - p3->x;
    double y13 = p1->y - p3->y;
    
    double x23 = p2->x - p3->x;
    double y23 = p2->y - p3->y;
    
    double x1p = p1->x - p->x;
    double y1p = p1->y - p->y;
    
    double x2p = p2->x - p->x;
    double y2p = p2->y - p->y;
        
    return ((x13*x23 + y13*y23)*(x2p*y1p - x1p*y2p))<((x23*y13 - x13*y23)*(x2p*x1p + y1p*y2p));
 
 /*   double cos_a = x13*x23 + y13*y23;
    double cos_b = x2p*x1p + y2p*y1p;
    
    if(cos_a >= 0 && cos_b >= 0)
        return false;
    
    if(cos_a < 0 && cos_b < 0)
        return true;
    
    double sin_ab = (x13*y23 - x23*y13) * cos_b + (x2p*y1p - x1p*y2p) * cos_a;
    
    if(sin_ab < 0)
        return true;
    
    return false;*/
}

static inline int is_coords_flipped(POINT_2D *p1,POINT_2D *p2)
{
    return p1 > p2;
}



/*Check on wich side point P is of line va->vb*/
static inline int checkside(POINT_2D *va, POINT_2D *vb, POINT_2D *p)
{
    double res;
    POINT_2D p1, p2;
    
    p1.x = vb->x - va->x;
    p1.y = vb->y - va->y;
    p2.x = p->x - va->x;
    p2.y = p->y - va->y;
        
    res = p1.x * p2.y - p1.y * p2.x; 
    if(res > 0)
        return 1;
    else if (res < 0)
        return -1;
    else
        return 0;
}

/*Check if there is a replacement point and if so, use it*/
static inline int check_point(POINT_2D **p)
{
    POINT_2D *po = *p;
    if(po->use_instead)
    {
        *p = po->use_instead;
        return 1;
    }
    return 0;    
}

/*******        MEMORY FUNCTIONS      *****************************/
/*This is just some functions to put allocation error checking at one place*/
 static inline void* st_malloc(size_t len)
{
#ifdef __PG__
    void *res = palloc(len);
#else    
    void *res = malloc(len);
#endif
    if(res==NULL)
    {
        fprintf(stderr, "Failed to allocate memory in function %s\n", __func__);
        exit(EXIT_FAILURE);
    }
    return res;
}


/**
 *Memory handling
 * Just a function handling allocation error on realloc
 */
 static inline void* st_realloc(void *ptr, size_t len)
{

#ifdef __PG__
    void *res = palloc0(sizeof(ptr) * len);
    if(res)
    {
        memcpy(res, ptr, len);
        pfree(ptr);
    }
#else    
    void *res = realloc(ptr, len);
#endif
    if(res==NULL)
    {
        free(ptr);
        fprintf(stderr, "Failed to allocate memory in function %s\n", __func__);
        exit(EXIT_FAILURE);
    }
    return res;
}


/**
 *Memory handling
 * Just a function handling allocation error on calloc
 */
 static inline void* st_calloc(int n, size_t s)
{
#ifdef __PG__
    void *res = palloc0(n * s);
#else    
    void *res = calloc(n, s);
#endif
    if(res==NULL)
    {
        fprintf(stderr, "Failed to allocate memory in function %s\n", __func__);
        exit(EXIT_FAILURE);
    }
    return res;
}

static inline void st_free(void *ptr)
{
#ifdef __PG__
    pfree(ptr);
#else    
    free(ptr);
#endif
    
}

/*******        DEBUG FUNCTION      *****************************/


static inline int print_tri(TRI *t)
{
    
            if(t)
            {
                POINT_2D *p1 = t->edges[0]->p[t->flipped[0]];
                POINT_2D *p2 = t->edges[1]->p[t->flipped[1]];
                POINT_2D *p3 = t->edges[2]->p[t->flipped[2]];
 //               log_this("TRIANGEL status: %d,p1: %lf, %lf, p2: %lf, %lf, p3: %lf, %lf\n flipped,%d, %d, %d\n",t->status,p1->x, p1->y,p2->x, p2->y,p3->x, p3->y,t->flipped[0],t->flipped[1],t->flipped[2]  );
                  log_this(",('polygon((%lf %lf,%lf %lf,%lf %lf, %lf %lf))'::geometry,0,0,0)\n\n",p1->x, p1->y,p2->x, p2->y,p3->x, p3->y,p1->x, p1->y);
            }
            return 0;
}

static inline int print_edge(EDGE *e)
{
        if(e)
        {
        	POINT_2D *p1 = e->p[0];
            POINT_2D *p2 = e->p[1];
        //    log_this("EDGE status: %d,p1: %lf, %lf, p2: %lf, %lf, %p\n",e->status,p1->x, p1->y,p2->x, p2->y, e);
           log_this("values('linestring(%lf %lf,%lf %lf)'::geometry)\n",p1->x, p1->y,p2->x, p2->y);
        }
            return 0;
}

static inline int print_all_triangels(TRI_LIST *t_list)
{
    int i;
    
    uint32_t alloced = 2048;
    char *mp = st_calloc(alloced, sizeof(char));
    
    printf("values\n");
    int started = 0;
    for (i=0;i<t_list->tri_list_used;i++)
    {
        TRI *t = t_list->tri_list + i;
        
            POINT_2D *p1 = t->edges[0]->p[t->flipped[0]];
            POINT_2D *p2 = t->edges[1]->p[t->flipped[1]];
            POINT_2D *p3 = t->edges[2]->p[t->flipped[2]];
            int side = checkside(p2, p1, p3);
            int err = 0;
            if(p1 == p2 || p2 == p3 || p3 == p1)
                err = 1;
            if(p3 != t->edges[1]->p[!t->flipped[1]] || p1 != t->edges[2]->p[!t->flipped[2]] || p2 != t->edges[0]->p[!t->flipped[0]])
                err = 2;
            if(started)
            {
                 log_this(",('POLYGON((%lf %lf,%lf %lf,%lf %lf,%lf %lf))'::geometry, %d, %d, %d)\n",p1->x, p1->y, p2->x, p2->y, p3->x, p3->y, p1->x, p1->y, t->status, side, err);
            }
            else
            {
                 started = 1;
                
                log_this("('POLYGON((%lf %lf,%lf %lf,%lf %lf,%lf %lf))'::geometry, %d, %d, %d)\n",p1->x, p1->y, p2->x, p2->y, p3->x, p3->y, p1->x, p1->y, t->status, side, err);
            }
 
    }
    
    
        st_free(mp);
        return 0;
        
}

static inline int print_used_in(POINT_2D *p)
{
    int n_used_in = p->n_used_in;
    
    int i;
    log_this("--------------------------POINT %lf, %lf have %d n_used_in addr %p------------------------\n",p->x, p->y, n_used_in, p->used_in);
    for (i=0;i<n_used_in;i++)
    {
        EDGE *e = p->used_in[i];
        
        log_this("used_in %d, p1: %lf, %lf, p2: %lf, %lf from edge-addr %p\n",i, e->p[0]->x, e->p[0]->y, e->p[1]->x, e->p[1]->y, e);
        
    }
    return 0;
}


static inline int print_all_points(POINTLIST *plist)
{    
    int i;
    for (i=-3;i<plist->tot_npoints;i++)
    {
        POINT_2D *p = plist->points + i;
        
//        printf("\n---used_in addr = %p and *e addr = %p \n\n", p->used_in, e);
        log_this("p %d: %lf, %lf\n",i,p->x, p->y);
        
    }
    return 0;
}


void print_point2file(POINT_2D *p, int id, char *te)
{
    
    FILE *f = fopen("/tmp/point.csv", "a");
if (f == NULL)
{
    printf("Error opening file!\n");
    exit(1);
}

            if(p)
            {
                fprintf(f, "point(%lf %lf);%d;%s\n",p->x, p->y, id, te);
            }
            

fclose(f);
}
void print_edge2file(EDGE *e, int id, char *te)
{
    
    FILE *f = fopen("/tmp/edge.csv", "a");
if (f == NULL)
{
    printf("Error opening file!\n");
    exit(1);
}

            if(e)
            {
                POINT_2D *p1 = e->p[0];
                POINT_2D *p2 = e->p[1];
                fprintf(f, "linestring(%lf %lf,%lf %lf);%d;%s\n",p1->x, p1->y,p2->x, p2->y, id, te);
            }
            

fclose(f);
}

void print_tri2file(TRI *t, int id, char *te)
{
    
    FILE *f = fopen("/tmp/tri.csv", "a");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        exit(1);
    }

            if(t)
            {
                POINT_2D *p1 = t->edges[0]->p[t->flipped[0]];
                POINT_2D *p2 = t->edges[1]->p[t->flipped[1]];
                POINT_2D *p3 = t->edges[2]->p[t->flipped[2]];
                fprintf(f, "polygon((%lf %lf,%lf %lf,%lf %lf, %lf %lf));%d;%s\n",p1->x, p1->y,p2->x, p2->y,p3->x, p3->y,p1->x, p1->y, id, te);
            }            

    fclose(f);
    print_tri(t);
}

void print_all_tri2file(TRI_LIST *t_list)
{
    
    FILE *f = fopen("/tmp/all_tri.csv", "w");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        exit(1);
    }
    int i;
        for (i=0;i<t_list->tri_list_used;i++)
        {
            TRI *t = t_list->tri_list + i;
                if(t)
                {
                    POINT_2D *p1 = t->edges[0]->p[t->flipped[0]];
                    POINT_2D *p2 = t->edges[1]->p[t->flipped[1]];
                    POINT_2D *p3 = t->edges[2]->p[t->flipped[2]];
                    fprintf(f, "polygon((%lf %lf,%lf %lf,%lf %lf, %lf %lf));%d\n",p1->x, p1->y,p2->x, p2->y,p3->x, p3->y,p1->x, p1->y, i);
                }
                
        }
    fclose(f);
    print_all_triangels(t_list);
}




/*******        STRUCT HANDLING      *****************************/

POINTLIST* pointlist_init(int npoints)
{
    POINTLIST *plist = st_malloc(sizeof(POINTLIST));
    POINT_2D *points = st_calloc(npoints + 3, sizeof(POINT_2D));  //add space for super triangle
    plist->points = points + 3;  //the real points start after super triangle
    plist->points_alloced = npoints; //This is realitive to the real points. If we ever find a need to realloc points this must be handled
    plist->points_used = 0;
    plist->n_points_per_ring = NULL;
    plist->n_points_per_ring_alloced = 0;
    plist->n_rings = 0;
    plist->point_in_ring_val = 0;
    plist->prealloced_used_in = NULL;
    plist->tot_npoints = npoints;
    plist->maxXY[0] = plist->maxXY[1] = __DBL_MIN__;
    plist->minXY[0] = plist->minXY[1] = __DBL_MAX__;
    
    plist->prealloced_used_in = st_calloc(PRE_ALLOCED_USED_IN * (npoints + 3), sizeof(EDGE*));
    return plist;
}

static inline int plist_destroy(POINTLIST *plist)
{
    int j;
    for (j = -3; j < plist->tot_npoints; j++ )
    {
        
        POINT_2D *p = plist->points + j;
        if(p->edgelist2b_freed)
        {
    //        printf("free %d bytes from %p, %lf, %lf\n", p->alloced_used_in, p->used_in, p->x, p->y);
            st_free(p->used_in);
        }
    }
    
    st_free(plist->points - 3);
    plist->points_alloced = 0;
    plist->points_used = 0;
    plist->points = NULL;
    st_free(plist->prealloced_used_in);
    st_free(plist->n_points_per_ring);
    st_free(plist->point_in_ring_val);
    st_free(plist);
    plist = NULL;
    
    return 0;
}


static inline TRI_LIST* tri_list_init(size_t n)
{
    TRI_LIST *tlist = st_malloc(sizeof(TRI_LIST));
    tlist->tri_list = st_calloc(n, sizeof(TRI));
    tlist->tri_list_alloced = n;
    tlist->tri_list_used = 0;
    
    return tlist;
}


static inline int tri_list_destroy(TRI_LIST *tlist)
{
    
    st_free(tlist->tri_list);
    tlist->tri_list_alloced = 0;
    tlist->tri_list_used = 0;
    st_free(tlist);
    tlist = NULL;
    
    return 0;
}




static inline EDGE_LIST* edge_list_init(size_t n)
{
    EDGE_LIST* edgelist = st_malloc(sizeof(EDGE_LIST));
    edgelist->edges = st_calloc(n, sizeof(EDGE));
    edgelist->e_list_alloced = n;
    edgelist->e_list_used = 0;
    
    return edgelist;
}


static inline int edge_list_destroy(EDGE_LIST *elist)
{
    
    st_free(elist->edges);
    elist->e_list_alloced = 0;
    elist->e_list_used = 0;
    st_free(elist);
    elist = NULL;
    
    return 0;
}

/*A function to register an edge as used by a point*/
static inline int register_edge_in_point(EDGE *e,POINT_2D *p)
{
    int i;
    for (i=0;i < p->n_used_in;i++)
    {
        if(e == p->used_in[i])
            return 0;
    }

    if(p->n_used_in == PRE_ALLOCED_USED_IN && !(p->edgelist2b_freed)) 
    {
        size_t new_size = 2 * PRE_ALLOCED_USED_IN;
        EDGE* *old_p_used_in = p->used_in;
        p->used_in = (EDGE**) st_calloc(new_size, sizeof(EDGE*));
        p->alloced_used_in = new_size;
        p->edgelist2b_freed = true;
        
        memcpy(p->used_in, old_p_used_in, PRE_ALLOCED_USED_IN * sizeof(EDGE*));
    }
    else if(p->n_used_in >= p->alloced_used_in && p->edgelist2b_freed)
    {
        size_t new_size = 2 * p->alloced_used_in;
        p->used_in = st_realloc(p->used_in, new_size * sizeof(EDGE*));        
        p->alloced_used_in = new_size;        
    }
    
    p->used_in[p->n_used_in] = e;
    
    p->n_used_in++;
    return 0;
}

/*If a point is removed from an edge when swapping the diagonal
 * we have to remove that edge from the lised of "used_in" for that point*/
static inline int remove_edge_from_point(EDGE *e,POINT_2D *p)
{
    int i;
    for (i=0;i < p->n_used_in;i++)
    {
        if(e == p->used_in[i])
        {
            if(i < p->n_used_in - 1)
                p->used_in[i] = p->used_in[p->n_used_in - 1]; //compact the list, put last edge in place of edge to be removed
            p->used_in[p->n_used_in - 1] = NULL;
            
            p->n_used_in--;
            return 0;
        }
    }
    return 1; //edge not found in point

}


/**************************************************************************************
*******************     EDGE Functions     ******************************************
***************************************************************************************/


/*This is function that is used in multiple ways. Should meybe be spolitted in separate functions for clarity
    1)  We have two points and a triangle and a position for the edge. THen we construct a new edge and add it
        Then the order of the points determins if the adde triangle for that edge will be 0 or 1
        since the point order is the correct point order in the triangle we add it to, and opints are always stored 
        in boundary order in the edge. So if p1 to the function has a greater address than p2, it is used flipped 
        in the triangle and the triangle is registered as tri[1] and vice verse
    2)  If also an edge is delivered to the function then it is an edge that we are using for 
        swapping the diagonal. That means that we replace the points in the edge with the new points
        meaning we have to unregister the edge from the old points and register the edge to the new points
    3)  If we don't get the points, just the triangle and an edge and a position to put the edge in
        we just put the edge in that position in the triangle. The calling function is responsible for
        keepiing a handler to the edge we take the place from and add tthat edge on correct place.    
    */
static inline EDGE* add_edge2tri(EDGE_LIST *edges, POINT_2D *p1, POINT_2D *p2, TRI *tri, uint8_t edge_pos, EDGE *edge,TRI *tri2replace)
{
    EDGE *e = NULL;
    if(edge && (!p1 || !p2))
    {
        e = edge;
        if(edge->tri[0] == tri2replace && edge->tri[1] != tri2replace)
        {
            e->tri[0] = tri;
            e->edge_pos_in_tri[0] = edge_pos;
            tri->flipped[edge_pos] = 0;
        }
        else if (edge->tri[1] == tri2replace && edge->tri[0] != tri2replace)
        {
            e->tri[1] = tri;
            e->edge_pos_in_tri[1] = edge_pos;        
            tri->flipped[edge_pos] = 1;
        } 
        else
        {
            log_this("Something is wrong. We had an triangle to replace that didn't exist in function %s\n", __func__);
            return NULL;
        }
    }
    else if(p1 && p2)
    {
        if(edge) //we have both points and an edge, lets replace the points in the edge
        {
            remove_edge_from_point(edge,edge->p[0]);
            remove_edge_from_point(edge,edge->p[1]);
            edge->tri[0] = NULL;
            edge->tri[1] = NULL;
            e = edge;
        }
        else if(edges)
        {
            if(edges->e_list_used >= edges->e_list_alloced)
            {
                log_this("We have a major problem, more edges than we calculated on");
                return NULL;
            }
            e = edges->edges + edges->e_list_used; //get first unused edge
            edges->e_list_used++;
        }
        
        if(p1 < p2)
        {
            e->p[0] = p1;
            e->p[1] = p2;
            e->tri[0] = tri;
            e->tri[1] = NULL;
            e->edge_pos_in_tri[0] = edge_pos;
            tri->flipped[edge_pos] = unflipped;
        }
        else
        {
            e->p[0] = p2;
            e->p[1] = p1;
            e->tri[0] = NULL;
            e->tri[1] = tri;
            e->edge_pos_in_tri[1] = edge_pos;
            tri->flipped[edge_pos] = flipped;
        } 
        register_edge_in_point(e,p1);
        register_edge_in_point(e,p2); 
    }
    else
    {
            log_this("Something is wrong. We cannot create a new edge without points in %s\n", __func__);
            return NULL;        
    }
    tri->edges[edge_pos] = e;
    
    return e;
}

/**************************************************************************************
*******************    TEMP EDGE Functions     ******************************************
***************************************************************************************/
/*Those temp edges is used to temporarary store lists of intersecting edges and
 * newly created edges from boundary restauration waiting for handling*/

static inline TEMP_EDGES_LIST* init_temp_edges()
{
    TEMP_EDGES_LIST *e = st_malloc(sizeof(TEMP_EDGES_LIST));
    e->edges = st_malloc(4 * sizeof(EDGE*));
    e->alloced_edges = 4;
    e->used_edges = 0;
    return e;
}


static inline int add_to_temp_edges(TEMP_EDGES_LIST *elist, EDGE *e)
{
    if(elist->used_edges >= elist->alloced_edges)
    {
        size_t new_size = 2 * elist->alloced_edges;
        elist->edges = st_realloc(elist->edges, new_size * sizeof(EDGE*));
        elist->alloced_edges = new_size;
    }
    
    *(elist->edges + elist->used_edges) = e;
    elist->used_edges++;
    return 0;
    
}
static inline int reset_temp_edges(TEMP_EDGES_LIST *elist)
{
    elist->used_edges = 0;
    return 0;
}

static inline int destroy_temp_edges(TEMP_EDGES_LIST *e)
{
    st_free(e->edges);
    e->edges = NULL;
    e->alloced_edges = 0;
    e->used_edges = 0;
    st_free(e);
    e = NULL;
    return 0;
}



/**************************************************************************************
*******************    Triangel manupulation functions     ****************************
***************************************************************************************/
/*Those functions is used to travel the triangle in differnt directions and find triangles on 
 * opposite side of edges and so on. The functions gets a generic structure that can hols 
 * data for all those different usages
 * THose function could probably be reduced and optimized. They are used a lot so it is worth 
 saving some cycles here*/

/*get an edge and a triangle and replace the triangle with the triangle on the opposite side of that edge*/
static inline int get_other_side_tri(TEMP_EDGE *te)
{
    TRI *t = te->tri;
    EDGE *e = te->edge;
    if(t == e->tri[0])
    {
        te->tri = e->tri[1];
        te->edge_location = e->edge_pos_in_tri[1];
        te->flipped = true;
        return 0;        
    }
     if(t == e->tri[1])
    {
        te->tri = e->tri[0];
        te->edge_location = e->edge_pos_in_tri[0];
        te->flipped = false;
        return 0;        
    }
    return 1; //something wrong with input TEMP_EDGE    
}

/*standing at point p looking along the edge. Get the triangle on the left side*/
static inline int get_left_side_tri(TEMP_EDGE *te, POINT_2D *p)
{
    EDGE *e = te->edge;
    
    if(e->p[0] == p)
    {
        te->tri = e->tri[0];
        te->flipped = false;
        te->edge_location = e->edge_pos_in_tri[0];
        return 0;
    }   
   else if(e->p[1] == p)
    {
        te->tri = e->tri[1];
        te->flipped = true;
        te->edge_location = e->edge_pos_in_tri[1];
        return 0;
    }   
    return 1; //something wrong with input TEMP_EDGE   
}

/*standing at point p looking along the edge. Get the edge on the left starting at the same point. Also return if the found edge is flipped from the perspective of the new triangle*/
static inline int get_edge_to_left(EDGE **e, POINT_2D *p)
{
    int left_e_loc;
    TRI *t;
    if((*e)->p[0] == p)
    {
        t = (*e)->tri[0];
        left_e_loc = prev_edge[(*e)->edge_pos_in_tri[0]];
        
    }   
   else if((*e)->p[1] == p)
    {
        t = (*e)->tri[1];
        left_e_loc = prev_edge[(*e)->edge_pos_in_tri[1]];
    }       
    else 
        return -1;
    
    *e = t->edges[left_e_loc];
    return t->flipped[left_e_loc];
}

/*get an edge and a triangle and replace the triangle with the opposite triangle from the edge
and also return the point from the new triangle that is not part of the edge */
static inline int get_oposite_tri_oposite_point(TEMP_EDGE *te)
{
    get_other_side_tri(te);
    TRI *t = te->tri;
    //Get next edge
    int new_loc = next_edge[te->edge_location];
    EDGE *e = t->edges[new_loc];
    //get second point in that edge
    te->p = e->p[!t->flipped[new_loc]];
    return 0;
}
/*Get an edge and a triangle and get the point that is not part of the edge*/
static inline int get_oposite_point(TEMP_EDGE *te)
{
    TRI *t = te->tri;
    //Get next edge
    int new_loc = next_edge[te->edge_location];
    EDGE *e = t->edges[new_loc];
    //get second point in that edge
    te->p = e->p[!t->flipped[new_loc]];
    return 0;
}


/*from the edge and triangle delivered, get the previous edge in that triangle always counter clock wise*/
static inline int get_prev_edge(TEMP_EDGE *te)
{
    TRI *t = te->tri;
    int new_edge_location = prev_edge[te->edge_location];
    EDGE *e = te->edge = t->edges[new_edge_location];
    te->edge_location = new_edge_location;
    
    if(t == e->tri[0])
        te->flipped = false;
    else if(t == e->tri[1])
        te->flipped = true;
    else    
        return 1; //something wrong with input TEMP_EDGE  
        
    return 0;
}

/*from the edge and triangle delivered, get the next edge in that triangle always counter clock wise*/
static inline int get_next_edge(TEMP_EDGE *te)
{
    TRI *t = te->tri;
    int new_edge_location = next_edge[te->edge_location];
    EDGE *e = te->edge = t->edges[new_edge_location];
    te->edge_location = new_edge_location;
    
    if(t == e->tri[0])
        te->flipped = false;
    else if(t == e->tri[1])
        te->flipped = true;
    else    
        return 1; //something wrong with input TEMP_EDGE  
        
    return 0;
}






/**************************************************************************************
*******************     LOAD POINTS     ******************************************
***************************************************************************************/

/*Here we add the incomming origianl points to the list that we will work from further on*/


int plist_add_ring(POINTLIST *plist, double *coordinates,int n_points)
{
    int i;
    POINT_2D *p;
    double maxx = __DBL_MIN__;
    double maxy = __DBL_MIN__;
    double minx = __DBL_MAX__;
    double miny = __DBL_MAX__;
    POINT_2D *left_upper = NULL;
    
    int start_at = plist->points_used;
    
    /*Find the bounding box to decide the size of the super triangle
     * and find the left upper corner of each ring to find rotation direction
     * This is done for each ring so we could save a few cycles by 
     * not checking the full box for inner rings*/
    for (i=0;i<n_points;i++)
    {
        p = plist->points + start_at + i ;
        p->x = *(coordinates + i*2);
        p->y = *(coordinates + i*2 + 1);
        p->used_in = plist->prealloced_used_in + (start_at + i) * PRE_ALLOCED_USED_IN;
        p->alloced_used_in = PRE_ALLOCED_USED_IN;
        p->edgelist2b_freed = false;
        if(p->x <= minx)
        {
            if(p->x < minx)
            {
                left_upper = p;
                minx = p->x;
            }
            else if (p->y > left_upper->y)                
                left_upper = p;           
        }
        if(!plist->n_points_per_ring) //if this is outer ring (we havent yet allocated space for n_rings, then find bbox*/
        {
            if(p->x > maxx)
                maxx= p->x;
            if(p->y > maxy)
                maxy= p->y;
            if(p->y < miny)
                miny = p->y; 
        }
    }
    if(!plist->n_points_per_ring) //if this is outer ring (we havent yet allocated space for n_rings, then find bbox*/
    {
        if(maxx > plist->maxXY[0])
            plist->maxXY[0] = maxx;
        if(maxy > plist->maxXY[1])
            plist->maxXY[1] = maxy;
        if(minx < plist->minXY[0])
            plist->minXY[0] = minx;
        if(miny < plist->minXY[1])
            plist->minXY[1] = miny;
    }
    
    /*Here we find out the direction of the ring.
     * First we have to connect first point with last point if the upper 
     * left corner is close to the beginning or the end*/
    POINT_2D *p0, *p1, *p2;
    p1 = left_upper;
    
    POINT_2D *firstpoint = plist->points + start_at;
    POINT_2D *lastpoint = plist->points + start_at + n_points - 1;
    if( p1 == firstpoint) //if first point, the point before is last point
    {
        
        if(lastpoint->x == p1->x && lastpoint->y == p1->y) //first and last popint is the same (like a postgis polygon)
            p0 = lastpoint - 1;
        else
            p0 = lastpoint;
    }
    else
        p0 = p1 - 1;
    
    if(p1 == lastpoint) //p1 is last point
    { 
        if(firstpoint->x == p1->x && firstpoint->y == p1->y) //first and last point is the same (like a postgis polygon)
            p2 = firstpoint + 1;
        else
            p2 = firstpoint;
    }
    else
        p2 = p1 + 1;
       
    int point_in_ring_val = 0;
    if(plist->n_rings == 0)
        point_in_ring_val = checkside(p1, p0, p2);
    else        
        point_in_ring_val = -1 * checkside(p1, p0, p2);
    
    /*Register how many points this ring has*/
    if(!plist->n_points_per_ring)
    {
        plist->n_points_per_ring = st_malloc(4 * sizeof(int));
        plist->point_in_ring_val = st_malloc(4 * sizeof(int));
        plist->n_points_per_ring_alloced = 4;
    }
    else if(plist->n_rings == plist->n_points_per_ring_alloced-1)
    {
        size_t new_size = 2 * plist->n_points_per_ring_alloced;
        plist->n_points_per_ring = st_realloc(plist->n_points_per_ring,new_size * sizeof(int));
        plist->point_in_ring_val = st_realloc(plist->point_in_ring_val,new_size * sizeof(int));
        plist->n_points_per_ring_alloced = new_size;
    }
    
    /*Add a value telling on what side a point should be relative an edge to be inside.
     * That depends on both ring direction and if it is an outer ring or inner ring*/
    plist->point_in_ring_val[plist->n_rings] = point_in_ring_val;
    
    
    plist->n_points_per_ring[plist->n_rings] = n_points;    
    plist->n_rings++;
    
    
    plist->points_used += n_points;
    
    return 0;
}









/**************************************************************************************
*******************     FIRST STEP, TRIANGULATION     ******************************************
***************************************************************************************/

/*The below shouldn't be valid anymore since point in triangle is slightly changed. 
 * This function is not safe now. if an edge is the first edge of the triangle on both sides of the edge
 * and the edge direction is exactly against the point we are searching for
 * then I think this search will just jump between the two triangles and not get out of the loop
 * Need to put some protection against this. Might take a while to bump into the issue in reality, but it will be ugly
 
 But to catch unpredicted situations like the one described above we add a guard against to many iterations.
 We add a counter and if the counter gets higher that it can be in any situation we error handle.
 We count on that we never will need more steps than the number of points we have to find our triangle (which should be way to much
 even if it is the last point we add and the edge goes past the whole data set)
 The counter will be reset for each start of tri search in function triangulate*/

static inline TRI* search_tri(TRI *tri, POINT_2D *p)
{
    
    /*Check if number of iterations is so high so it must be an error*/
    if (iteration_counter++ > max_iterations)
        return NULL;
    int pip = point_in_triangle(tri, p);
    if(!pip || p->use_instead)
        return tri;
    
    EDGE *the_edge = tri->edges[pip-1];
    
    TRI *new_tri = the_edge->tri[!tri->flipped[pip-1]];
    return search_tri(new_tri, p);
    
}



static inline int restore_delaunay(EDGE_LIST *e_list,POINT_2D *p,TRI *t,int common_edge_index)
{
    /*Check if we are stuck in an infinite loop*/
    if(iteration_counter++ > max_iterations)
    {
        log_this("restore delaunay is stuck in an infinite loop, we abort\n");
        return 1;
    }
    int next_edge[3] = {1,2,0};
    /*The edge shared between the triangles. This is the edge to delete and replace*/
    EDGE *common_edge = t->edges[common_edge_index];
    /*This egde is use flipped by one of the triangles and unflipped by th other
     * here we find out if the first triangle (the one with the added point) uses it flipped or unflipped*/
    pointorder tri_flipped = t->flipped[common_edge_index];
    
    /*The other triangle is registered on the edge with the reveresed flippedness*/
    TRI *other_tri = common_edge->tri[!tri_flipped];
    int other_tri_edge1_pos = common_edge->edge_pos_in_tri[!tri_flipped];
    if(other_tri && swap_test(other_tri, p, other_tri_edge1_pos))
    {
        int tri_edge1_pos = common_edge_index;
        int tri_edge2_pos = next_edge[tri_edge1_pos];
        int tri_edge3_pos = next_edge[tri_edge2_pos];
        
        int other_tri_edge2_pos = next_edge[other_tri_edge1_pos];
        int other_tri_edge3_pos = next_edge[other_tri_edge2_pos];
        

        /*here we get the second point from the edge after the common edge*/
        POINT_2D *furthest_point = other_tri->edges[other_tri_edge2_pos]->p[!other_tri->flipped[other_tri_edge2_pos]]; 
        /*Now start switching edges
         * We start by substituting the common edge with a new edge going from p to the furthest point*/
        
        EDGE *new_edge = add_edge2tri(e_list,p,  furthest_point, t, tri_edge1_pos, t->edges[common_edge_index], NULL); //tri_edge1 we have defined as the common edge;
        if(!new_edge)
        {
            log_this("Failed to create new edge when swapping edges\n");
            return 1;
        }
        
        if(!add_edge2tri(NULL,NULL,NULL, other_tri, other_tri_edge1_pos, new_edge, NULL))
        {
            log_this("Failed to move edge when swapping edges\n");
            return 1;
        }
        /*Ok now we need to swithch the 2 other edges too.
         * What we want is the third from the other triangel in the second position 
         * and the second from the same triangle in the third position
         * So, we need to store the info about the second edge to not overwrite it.
         * What we need to store are 
         * 1) the address to the edge
         * */
        
        EDGE *tri_edge2 = t->edges[tri_edge2_pos];
        EDGE *other_tri_edge2 = other_tri->edges[other_tri_edge2_pos];
        EDGE *tri_edge3 = t->edges[tri_edge3_pos];
        EDGE *other_tri_edge3 = other_tri->edges[other_tri_edge3_pos];
        
        
        /*Now let's put those edges where they belong*/
        int err = 0;
        if(!add_edge2tri(NULL,NULL,NULL, t, tri_edge2_pos, other_tri_edge3, other_tri))//in t position "2" add edge from other_tri and declare it is stolen from other_tri
            err = 1;
        if(!add_edge2tri(NULL,NULL,NULL, t, tri_edge3_pos, tri_edge2, t)) //in t position "3" add edge from same triangle and declare it origins from the same triangle
           err = 1;
        if(!add_edge2tri(NULL,NULL,NULL, other_tri, other_tri_edge2_pos, tri_edge3, t)) //in t position "2" add edge from other_tri and declare it is stolen from other_tri
           err = 1;
        if(!add_edge2tri(NULL,NULL,NULL, other_tri, other_tri_edge3_pos, other_tri_edge2, other_tri)) //in t position "3" add edge from same triangle and declare it origins from the same triangle
           err = 1;
        if(err)
        {
            log_this("Failed to move edge when swapping edges\n");
            return 1;
        }
        
        
        if(restore_delaunay(e_list, p, t, tri_edge2_pos))
            return 1;
        if(restore_delaunay(e_list, p, other_tri, other_tri_edge3_pos))
            return 1;
        
    }
    
    return 0;
    
}



/*This is where we add new points to the triangulation
 * this is done in several steps
 * 1) save pointers to the edges from the original triangel and the flip-list
 * 2) In the original triangle, leave the first edge as is
 * 3) find the most counter clockwise point in that edge (the second if not flipped)
 * 4) create an edge from that point to the new point and set it to flipped (if the new point is before the old point in the original list of points) or unflipped
 * 5) put the original triangle as tri1 if unflipped or tri2 otherwise
 * 6) put this new edge as edge 2 in the original triangel
 * 7) store the pointer to the new edge
 * 7) repeat 3-7 for the third edge in the new triangle (stored in the original triangle
 * 8) find a pointer to the first unused triangle (or allocate more triangels)
 * 9) repaat and store in new triangle, use already created edge if present or create new
 * */

static inline int add_new_point(TRI_LIST *t_list, EDGE_LIST *e_list,  TRI *orig_tri, POINT_2D *p)
{
    int err = 0;
    POINT_2D *p1_old, *p2_old;
    EDGE *new_edge1, *new_edge2, *new_edge3;
    
    EDGE *orig_edge1 = orig_tri->edges[0];
    EDGE *orig_edge2 = orig_tri->edges[1];
    EDGE *orig_edge3 = orig_tri->edges[2];
    
    int orig_flipped1 = orig_tri->flipped[0];
    int orig_flipped2 = orig_tri->flipped[1];
//    int orig_flipped3 = orig_tri->flipped[2];
    
        
    if(t_list->tri_list_used + 1 >= t_list->tri_list_alloced) //We need 2 new triangels
    {
        log_this("We have a major problem, more triangels than we calculated on");
        return 1;
    }
  
/*First new triangle we save in place of the original triangle
 * First edge we don't touch it shall remain as original*/ 

    TRI *tri1 = orig_tri;
    
    /*Ge first point for new edge*/

    p1_old = orig_edge1->p[orig_flipped1];
    p2_old = orig_edge1->p[!orig_flipped1];
  

    new_edge1 = add_edge2tri(e_list, p2_old, p, tri1, 1, NULL, NULL);
    new_edge3 = add_edge2tri(e_list, p, p1_old, tri1, 2, NULL, NULL);

    if(!new_edge1 || !new_edge3)
        err = 1;
    
/*Second triangle. Here we make a new one*/
    TRI *tri2 = t_list->tri_list + t_list->tri_list_used;
    t_list->tri_list_used++;
    /*Get the order right for the second original edge*/

    p2_old = orig_edge2->p[!orig_flipped2];

    
    if (!add_edge2tri(NULL,NULL,NULL, tri2, 0, orig_edge2, orig_tri))
        err = 1;
    new_edge2 = add_edge2tri(e_list,p2_old, p, tri2, 1, NULL, NULL);
    if(!new_edge2)
        err = 1;
    if(!add_edge2tri(NULL,NULL,NULL, tri2, 2, new_edge1, NULL))
        err = 1;
    
/*Third triangle*/
    TRI *tri3 = t_list->tri_list + t_list->tri_list_used;
    t_list->tri_list_used++;
    
    if (!add_edge2tri(NULL,NULL,NULL, tri3, 0, orig_edge3, orig_tri))
        err = 1;
    if (!add_edge2tri(NULL,NULL,NULL, tri3, 1, new_edge3, NULL))
        err = 1;
    if (!add_edge2tri(NULL,NULL,NULL, tri3, 2, new_edge2, NULL))
        err = 1;
     
    if (err)
    {
        log_this("Failed to move edges in function %s\n", __func__);
        return 1;
    }
    
/*Let's find out if we need to flip some triangels to obey the delaunay rules*/
    iteration_counter = 0;
    if(restore_delaunay(e_list,  p, tri1, 0))
    {
       log_this("Failed to restore_delaunay  %s\n", __func__);
        return 1;
    }
    iteration_counter = 0;
    if(restore_delaunay(e_list,  p, tri2, 0))
    {
       log_this("Failed to restore_delaunay  %s\n", __func__);
        return 1;
    }
    iteration_counter = 0;
    if(restore_delaunay(e_list,  p, tri3, 0))
    {
       log_this("Failed to restore_delaunay  %s\n", __func__);
        return 1;
    }

    
    return 0;
}




/**************************************************************************************
*******************     Solve intersections     ******************************************
***************************************************************************************/



static inline int remove_unused(TRI_LIST *tlist)
{
    int i, j;
    
    EDGE_LIST *elist = tlist->e_list;
    for ( i = 0 ; i < elist->e_list_used;i++)
    {
        
        
         EDGE *e = elist->edges + i;
         
 /*        if(e->status == boundary)
         {
            e->tri[0]->status = e->tri_status[0];
            e->tri[1]->status = e->tri_status[1];
         }
     */          
        
        if( e->status == outside || e->status == undefined)
        {        
            if(e->tri[0])
                e->tri[0]->status = unused;
            if(e->tri[1])
                e->tri[1]->status = unused;
        }
    }
      
      /*special case with triangle holes*/
      POINTLIST *plist = tlist->p_list;
      int accu_points = 0;
    for (j=0;j<plist->n_rings;j++)
    {
        int n_points = plist->n_points_per_ring[j];
        if (j > 0 && n_points <= 4)
        {
            POINT_2D *p1 = plist->points + accu_points;
            POINT_2D *p2 = p1+1;
            check_point(&p1);
            check_point(&p2);
            
            int z;
            int finnished =0;
            for(z=0; z<p1->n_used_in;z++)
            {
                EDGE *e = p1->used_in[z];
                if(p2 == e->p[1])
                {
                    if(plist->point_in_ring_val[j] > 0)
                    {
                        e->tri[0]->status = outside;
                    }
                    else
                    {
                        e->tri[1]->status = outside;
                    }
                    finnished = 1;
                    break;
                        
                }
                if(finnished)
                    break;
            }
            
        }
      
       
      accu_points += plist->n_points_per_ring[j];
    }
    
    
    return 0;
    
}

static inline int check_oposite_points(EDGE *e, POINT_2D *p1, POINT_2D *p2, int in_val,int less_than_180 )
{
    TRI *t1 = e->tri[0];
    int next_edge_1_pos = next_edge[e->edge_pos_in_tri[0]];
    EDGE *next_edge_1 = t1->edges[next_edge_1_pos];
    POINT_2D *p10 = next_edge_1->p[!t1->flipped[next_edge_1_pos]];
    TRI *t2 = e->tri[1];
    int next_edge_2_pos = next_edge[e->edge_pos_in_tri[1]];
    EDGE *next_edge_2 = t2->edges[next_edge_2_pos];
    POINT_2D *p20 = next_edge_2->p[!t2->flipped[next_edge_2_pos]];
    
    if((checkside(p20, p10, e->p[0]) * checkside(p20, p10, e->p[1])) < 0) 
    {
        //edge points is on oposite sides of line between triangel oposite points which means it is a convex triangle

        int prev_edge_1_pos = prev_edge[e->edge_pos_in_tri[0]];
        EDGE *prev_edge_1 = t1->edges[prev_edge_1_pos];
        int prev_edge_2_pos = prev_edge[e->edge_pos_in_tri[1]];
        EDGE *prev_edge_2 = t2->edges[prev_edge_2_pos];
                

        EDGE *new_edge;

        
        /*Now let's put those edges where they belong*/
        
        
        if( p10 < p20)
        {   
            new_edge = add_edge2tri(NULL,p10, p20, t1, e->edge_pos_in_tri[0], e, NULL); //tri_edge1 we have defined as the common edge;
            add_edge2tri(NULL,NULL,NULL, t2, e->edge_pos_in_tri[1], new_edge, NULL);
            
            add_edge2tri(NULL,NULL,NULL, t1, next_edge_1_pos, prev_edge_2, t2); //in t position "2" add edge from other_tri and declare it is stolen from other_tri
            add_edge2tri(NULL,NULL,NULL, t1, prev_edge_1_pos, next_edge_1, t1); //in t position "3" add edge from same triangle and declare it origins from the same triangle
            add_edge2tri(NULL,NULL,NULL, t2, next_edge_2_pos, prev_edge_1, t1); //in t position "2" add edge from other_tri and declare it is stolen from other_tri
            add_edge2tri(NULL,NULL,NULL, t2, prev_edge_2_pos, next_edge_2, t2); //in t position "3" add edge from same triangle and declare it origins from the same triangle
 
            if((p20 == p2 && p10 == p1) || (p20 == p1 && p10 == p2))
            {
                e->status = boundary;
                e->tri_status[0] = in_val;
                e->tri_status[1] = -1 * in_val;
            }
         }
        else
        {       
            new_edge = add_edge2tri(NULL,p20, p10, t1, e->edge_pos_in_tri[0], e, NULL); //tri_edge1 we have defined as the common edge;
            add_edge2tri(NULL,NULL,NULL, t2, e->edge_pos_in_tri[1], new_edge, NULL);
            
            add_edge2tri(NULL,NULL,NULL, t1, next_edge_1_pos, prev_edge_1, t1); //in t position "2" add edge from other_tri and declare it is stolen from other_tri
            add_edge2tri(NULL,NULL,NULL, t1, prev_edge_1_pos, next_edge_2, t2); //in t position "3" add edge from same triangle and declare it origins from the same triangle
            add_edge2tri(NULL,NULL,NULL, t2, next_edge_2_pos, prev_edge_2, t2); //in t position "2" add edge from other_tri and declare it is stolen from other_tri
            add_edge2tri(NULL,NULL,NULL, t2, prev_edge_2_pos, next_edge_1, t1); //in t position "3" add edge from same triangle and declare it origins from the same triangle
            
            if((p20 == p2 && p10 == p1) || (p20 == p1 && p10 == p2))
            {
                e->status = boundary;
                e->tri_status[1] = in_val;
                e->tri_status[0] = -1 * in_val;
            }
         }
        

        

        
        
        if(e->status != boundary)
        {
            //TODO check if all those tests are nessecary 
            if(p1 != p10 && p1 != p20 && p2 != p10 && p2 != p20 && (checkside(p2, p1, p20) * checkside(p2, p1, p10)) < 0) //if points is on oposite side of the line between p1 and p2, we still have to an find_intersections
                add_to_temp_edges(intersecting_edge_list,e); //add edge back            
            else
            {
                int check1 = checkside(p2, p1, p10);
                int check2 = checkside(p2, p1,  p20);
                if((check1 == in_val || check1 == 0) && (check2 == in_val || check2 == 0)) 
                {
                    e->status = inside;
                    add_to_temp_edges(new_edges_list, new_edge);//those are stored to check for delaunay-restauration later
                }
                else
                {
                    new_edge->status = outside;
                }

                
            }
        }
        
        
    }
    else
    {
        add_to_temp_edges(intersecting_edge_list,e); //add edge back
    }
    
    return 0;
}




static inline int restore_delaunay2(POINT_2D *p1,POINT_2D *p2,int in_val, int less_than_180)
{
    int i = 0;
   
    
    while (i < ((int) new_edges_list->used_edges) - 1)
    {
        EDGE *e = new_edges_list->edges[i];
        if (e->status == boundary)
        {
            i++;
            continue;
        }
        
        int check1 = checkside(p2, p1, e->p[0]);
        int check2 = checkside(p2, p1,  e->p[1]);
        if((check1 == in_val || check1 == 0) && (check2 == in_val || check2 == 0)) 
        {
            e->status = inside;
        }
        else
        {
            e->status = outside;
            i++;
            continue;
        }
        TRI *t1 = e->tri[0];
        TRI *t2 = e->tri[1];
        
        int edge_1_pos = e->edge_pos_in_tri[0];
        int edge_2_pos = e->edge_pos_in_tri[1];
        int next_edge_1_pos = next_edge[edge_1_pos];
        EDGE *next_edge_1 = t1->edges[next_edge_1_pos];
        POINT_2D *p10 = next_edge_1->p[!t1->flipped[next_edge_1_pos]];
        int next_edge_2_pos = next_edge[edge_2_pos];
        EDGE *next_edge_2 = t2->edges[next_edge_2_pos];
        POINT_2D *p20 = next_edge_2->p[!t2->flipped[next_edge_2_pos]];
    
    
    
        
        if (swap_test(t1, p20,edge_1_pos) || swap_test(t2, p10, edge_2_pos))
        {
            //We have to do the swap_test
            
                          
            EDGE *new_edge = add_edge2tri(NULL,p10, p20, t1, edge_1_pos, e, NULL); //tri_edge1 we have defined as the common edge;
            
            add_to_temp_edges(new_edges_list, new_edge);//those are stored to check for delaunay-restauration later
            add_edge2tri(NULL,NULL,NULL, t2, edge_2_pos, new_edge, NULL);
            
            int prev_edge_1_pos = prev_edge[edge_1_pos];
            EDGE *prev_edge_1 = t1->edges[prev_edge_1_pos];
            int prev_edge_2_pos = prev_edge[edge_2_pos];
            EDGE *prev_edge_2 = t2->edges[prev_edge_2_pos];
            
            
            /*Now let's put those edges where they belong*/
            
            add_edge2tri(NULL,NULL,NULL, t1, next_edge_1_pos, prev_edge_2, t2); //in t position "2" add edge from other_tri and declare it is stolen from other_tri
            add_edge2tri(NULL,NULL,NULL, t1, prev_edge_1_pos, next_edge_1, t1); //in t position "3" add edge from same triangle and declare it origins from the same triangle
            add_edge2tri(NULL,NULL,NULL, t2, next_edge_2_pos, prev_edge_1, t1); //in t position "2" add edge from other_tri and declare it is stolen from other_tri
            add_edge2tri(NULL,NULL,NULL, t2, prev_edge_2_pos, next_edge_2, t2); //in t position "3" add edge from same triangle and declare it origins from the same triangle       
            
        }
        i++;
        
    }
    return 0;
}

static inline int fix_intersections(POINT_2D *p1,POINT_2D *p2, int in_val, int less_than_180)
{
    
    int i = 0;
    
    while(i < intersecting_edge_list->used_edges)
    {
        /*check if we are in an infinite loop where new edges are added all the time*/
        if(iteration_counter++ > max_iterations)
        {
            log_this("Stuck in infinite loop when fixing intersections in function  %s\n", __func__);
            return 1;
        }
        EDGE *e = intersecting_edge_list->edges[i];
        if(e->status == boundary)
            log_this("We have a self intersection\n");
        check_oposite_points(e, p1, p2, in_val, less_than_180);
        i++;
    }
    
    restore_delaunay2(p1, p2, in_val, less_than_180);
    return 0;
    
}


static inline int find_intersections(TEMP_EDGE *te,POINT_2D *p1,POINT_2D *p2, int in_val, int less_than_180)
{
    
    /*Find if we are in an infinite loop and error handle if so*/
    if(iteration_counter++ > max_iterations)
    {
        log_this("Stuck in infinite loop when finding intersections in function  %s\n", __func__);
        return 1;
    }
    get_oposite_tri_oposite_point(te);
    
    if(te->p == p2)
        return 0;
    
    
    if(checkside(p2, p1, te->p) == 1)
        get_prev_edge(te);
    else
        get_next_edge(te);
    
    add_to_temp_edges(intersecting_edge_list,te->edge);
    if(find_intersections(te, p1, p2, in_val,less_than_180 )) 
        return 1; 
    
    return 0;

}




/**************************************************************************************
*******************     Restore polygon , constrain the triangulation     **************
***************************************************************************************/


static inline int restore_boundary(POINTLIST *plist, EDGE_LIST *elist)
{
    int i, j, accu_points = 0;
    int in;
    intersecting_edge_list = init_temp_edges();
    new_edges_list = init_temp_edges();
    for (i = 0; i < plist->n_rings;i++)
    {
        
        
        int npoints = plist->n_points_per_ring[i];
    
        POINT_2D *first_point, *last_point,*p0, *p1, *p2, *p;
        
        first_point = p1 = plist->points + accu_points;
        last_point = p0 = plist->points + accu_points + npoints - 1;
        if (p1->x == last_point->x && p1->y == last_point->y)
            last_point = p0 = plist->points + accu_points + npoints - 2;
        

        in = plist->point_in_ring_val[i];    
            
            
            for (j = 0; j< npoints; j++)
            {
                
               // print_all_triangels(global_tlist);
                reset_temp_edges(intersecting_edge_list);
                reset_temp_edges(new_edges_list);
                p1 = first_point + j;
                        

                int point_pair_finnished = false;

                if(p1 < last_point)
                    p2 = p1 + 1;
                else if (p1->x == first_point->x && p1->y == first_point->y)
                    continue;//connecting back to first point //last point is already same as first point so we are finnished
                else                    
                    p2 = first_point;
                
 
                check_point(&p0); 
                int replaced = check_point(&p1); 
                check_point(&p2);
                    
                /*define what edges point out of the polygon and who points in
                 *also find out if any of the edges is already a boundary edge 
                 *and if not what triangle points in the right direction 
                 *(meaning crossed by the boundary*/     
                int z;
                
                EDGE *e = p1->used_in[0];
                if(e->p[0] == p1)
                    p = e->p[1];
                else if (e->p[1] == p1)
                    p = e->p[0];
                else
                {
                    log_this("Problem in function %s\n",__func__);
                    return 1;
                }
                
                TEMP_EDGE te;
                te.edge = e;
                
                EDGE *last_side = NULL;
                int last_side_found = false;
                
                /*If this point is replacing another point, it means that there are 2 identical points (not start and end of ring)
                 * This is happening when we have a self intersection.
                 * This means that p1 might be connected to many boundarys.
                 * If we respect inside/outside between pairs of boundarys not belonging to current cycle, we should be safe*/
                
     
                
                /*check if corner to check is more than 180 degrees.
                 * If so it is enough that the point is on the inside of 1 of the legs*/
                int less_than_180 = 0;
                if(checkside(p1, p0, p2) == in)
                    less_than_180 = 1;
                for (z = 0; z < p1->n_used_in; z++)
                {
                  //  print_all_tri2file(global_tlist);
                    
                  //  print_edge2file(e,layer_counter++, "edge");
                    if(p == p2)
                    {
                        point_pair_finnished = true;
                        e->status = boundary;
                        if(i==0)
                        {
                            if(p1 > p2)
                            {
                                e->tri_status[0] = in;
                                e->tri_status[1] = -1 * in;
                            }
                            else
                            {
                                e->tri_status[1] = in;
                                e->tri_status[0] = -1 * in;
                            }
                        }
                    }
                    else
                    {       
                        int check1 = checkside(p1, p0, p) == in;
                        int check2 = checkside(p2, p1, p) == in;
                         if((check1 && check2) || (!less_than_180 && (check1 || check2)))
                        {
                            if(e->status != boundary && !replaced)
                                    e->status = inside;
                       }
                        else
                        {
                            if(e->status != boundary && !replaced)
                                   e->status = outside;
 
                        }
                        
                        if(check2)
                        {
                            if(in == 1)
                            {
                                if(!last_side_found)
                                    last_side = e;
                            }
                            else
                            {
                                if(last_side)
                                    last_side_found = true;                                  
                            }
 
                            
                        }
                        else
                        {
                            if(in == -1)
                            {
                                if(!last_side_found)
                                    last_side = e;
                            }
                            else
                            {
                                if(last_side)
                                    last_side_found = true;                                  
                            }      
                            
                        }
                        
                        
                                          
                    }

                   /* get_left_side_tri(&te, p1);
                    get_prev_edge(&te);
                    e = te.edge;
                    p = e->p[te.flipped]; //we are on the right side of the edge, and want the furthest point. if edge is flipped from this perspective we want the second point (1) and if it is not flipped we want the first point (0)
                 */
                    int new_flippedness = get_edge_to_left(&e, p1);
                    p = e->p[new_flippedness];
                }
                
                if(!point_pair_finnished)
                {
                    te.edge = last_side;
                    get_left_side_tri(&te, p1);
                    get_next_edge(&te);
                    
                    //ok, we know we have an intersecting edge since the left edge was inside the boundary and the right is outside. Then the middle edge must cross the boundary
                    add_to_temp_edges(intersecting_edge_list,te.edge);
                    
                    iteration_counter = 0;
                    if(find_intersections(&te, p1, p2, in, less_than_180)) //find if there are more intersections
                        return 1;
                    
                    iteration_counter = 0;
                    if(fix_intersections(p1, p2, in, less_than_180))
                        return 1;
                    
                }
                
                
                if(replaced)
                {   
                        
                    int n_boundaries = 0;    
                    uint8_t status = 0;// 1 means outside, 0 means inside
                    for (z = 0; z < p1->n_used_in; z++)
                    {
                        EDGE *e = p1->used_in[z];
                        if(e->status == boundary)
                            n_boundaries++;
                    }
                    if(n_boundaries >= 3)
                    {
                        /* Ok, we have come to a crossing or intersection.
                         * Then we can determine if it is a crossing or just intersection
                         Since this is a hopefully rare special case it doesn't matter so much if this part is not fully optimized*/
                                   
                        
                        TEMP_EDGE te_special;                        
                        te_special.edge = e;
                        POINT_2D *p1_special = p, *p2_special;
                        
                        if(e->p[0] == p1)
                            p2_special = e->p[0];
                        else if (e->p[1] == p1)
                            p2_special = e->p[1];
                
                        int counter = 0;
                        int start_found = 0;

                 //   print_all_tri2file(global_tlist);
                    
                        for (z = 0; z < 2 * p1->n_used_in; z++)
                        {
                            
                  //  print_edge2file(e,layer_counter++, "edge");
                                if(e->status == boundary)
                                {                                
                                    if(p1_special == p0)  
                                    {
                                        counter = 0;
                                        start_found = 1;
                                        if((i == 0 && in > 0) || (i > 0 && in < 0))                                            
                                            status = 1;
                                        else
                                            status = 0;
                                    }
                                    else if(start_found)
                                    {
                                        if(p1_special == p2)
                                        {
                                            if(counter == 1 || counter == 3)
                                            {
                                                log_this("Ok, we have a crossing at point %lf, %lf, this is not handled yet\n", p1->x, p1->y);
                                                return 1;
                                            }
                                            counter = 0;
                                        }
                                        else
                                            counter++;
                                        status = status^1;
                                        
                                    }
                                }
                            if(start_found && e->status != boundary)
                            {
                                if(status == 0)
                                    e->status = inside;
                               /* else
                                    e->status = outside;
*/
                            }
  /*                              get_left_side_tri(&te_special, p1);
                                get_prev_edge(&te_special);
                                e = te_special.edge;
                                p1_special = e->p[te_special.flipped]; //we are on the right side of the edge, and want the furthest point. if edge is flipped from this perspective we want the second point (1) and if it is not flipped we want the first point (0)
                                p2_special = e->p[!te_special.flipped]; //we are on the right side of the edge, and want the furthest point. if edge is flipped from this perspective we want the second point (1) and if it is not flipped we want the first point (0)
*/
                                int new_flippedness = get_edge_to_left(&e, p1);
                                p1_special = e->p[new_flippedness];
                                p2_special = e->p[!new_flippedness];
                                
                        }
                    }
                }
                
                
                
                p0 = first_point + j;
            }
        
        accu_points += npoints;
        
    }
    destroy_temp_edges(intersecting_edge_list);
    destroy_temp_edges(new_edges_list);
    
    remove_unused(global_tlist);
    
    return 0;    
}



static inline int init_super_tri(POINTLIST *plist, TRI_LIST *t_list, EDGE_LIST *e_list)
{
    
    TRI *first_tri = t_list->tri_list;
    double maxdist, supertri_centerx, supertri_centery;
    POINT_2D *p;
    
    double maxx = plist->maxXY[0];
    double maxy = plist->maxXY[1];
    
    double minx = plist->minXY[0];
    double miny = plist->minXY[1];
    
    int n_points = plist->tot_npoints;
    
    if(maxx-minx>maxy-miny)
        maxdist = maxx-minx;
    else
        maxdist = maxy-miny;
    
    
    supertri_centerx = minx + (maxx-minx)/2;
    supertri_centery = maxy;
    
    
    p = plist->points - 3;
    p->x = supertri_centerx - 5 * maxdist;
    p->y = supertri_centery - 5 * maxdist;
    p->used_in = plist->prealloced_used_in + n_points * PRE_ALLOCED_USED_IN;
    p->alloced_used_in = PRE_ALLOCED_USED_IN;
    p->edgelist2b_freed = false;
    
    p = plist->points - 2;    
    p->x = supertri_centerx + 5 * maxdist;
    p->y = supertri_centery - 5 * maxdist;
    p->used_in = plist->prealloced_used_in + n_points * PRE_ALLOCED_USED_IN + PRE_ALLOCED_USED_IN;
    p->alloced_used_in = PRE_ALLOCED_USED_IN;
    p->edgelist2b_freed = false;
    p = plist->points - 1;
    p->x = supertri_centerx;
    p->y = supertri_centery + 5 * maxdist;  
    p->used_in = plist->prealloced_used_in + n_points * PRE_ALLOCED_USED_IN + 2 * PRE_ALLOCED_USED_IN;
    p->alloced_used_in = PRE_ALLOCED_USED_IN;
    p->edgelist2b_freed = false;
    
    
    
    e_list->edges[0].p[0] = plist->points - 3;
    e_list->edges[0].p[1] = plist->points - 2;
    e_list->edges[0].edge_pos_in_tri[0] = 1;
    e_list->edges[0].tri[0] = first_tri;
    e_list->edges[0].tri[1] = NULL;
    
    e_list->edges[1].p[0] = plist->points - 2;
    e_list->edges[1].p[1] = plist->points - 1;
    e_list->edges[1].edge_pos_in_tri[0] = 1;
    e_list->edges[1].tri[0] = first_tri;
    e_list->edges[1].tri[1] = NULL;
    
    e_list->edges[2].p[0] = plist->points - 3;
    e_list->edges[2].p[1] = plist->points - 1;
    e_list->edges[2].edge_pos_in_tri[0] = 1;
    e_list->edges[2].tri[0] = NULL;
    e_list->edges[2].tri[1] = first_tri;
    e_list->e_list_used = 3;
       
    first_tri->edges[0] = e_list->edges + 0;
    first_tri->flipped[0] = unflipped;
    first_tri->edges[1] = e_list->edges + 1;
    first_tri->flipped[1] = unflipped;
    first_tri->edges[2] = e_list->edges + 2;
    first_tri->flipped[2] = flipped;
    t_list->tri_list_used = 1;
    return 0;
    
}


static int triangulate(POINTLIST *plist, TRI_LIST *t_list, EDGE_LIST *e_list,int constrained)
{
    
    
    TRI *tri;
    POINT_2D *p;
    int pointnumber, n_in_ring,np = 0, ringnumber = 0;
    layer_counter = 0;
    global_tlist = t_list;
    t_list->e_list = e_list;
    t_list->p_list = plist;
    init_super_tri(plist, t_list, e_list);
    
    /*found that number of delaunay restore iterations actually can be a little more than the number ofinput points
     * So we increase the allowed number of iterations. If nessecary we can multiply it by 10, it won't matter.
     * It will still guard againste infinite looping*/
    max_iterations = plist->tot_npoints * 10;
     
    tri = t_list->tri_list + 0; //the super triangle
    n_in_ring = plist->n_points_per_ring[0];
    POINT_2D *first_point_in_ring = plist->points;

    for (pointnumber = 0; pointnumber < plist->points_used;pointnumber++)
    {
        
        p = plist->points + pointnumber;
        
        np++;
        if(np >= n_in_ring)
        {
            np = 0;
            int ignore_this_point = 0;
            if(p->x == first_point_in_ring->x && p->y == first_point_in_ring->y)
                ignore_this_point = 1;         
            ringnumber++;
            if(ringnumber < plist->n_rings)
                n_in_ring = plist->n_points_per_ring[ringnumber];
            first_point_in_ring = p + 1;   
            
            if(ignore_this_point)
                continue;

        }
        
        /*We reset the counter used to catch if the search for triangles goes into an infinite loop*/
        iteration_counter = 0;
        
        tri = search_tri(tri, p);
        
        if(!tri)
        {
            log_this("Failed to search for point in triangle\n");
            return 1;
        }
        //If use_instead is set, this point is already in the triangulation and we ignore this. For restoring the boundary we use the "use_instead" point instead
        if(p->use_instead)
            continue;
        
        iteration_counter = 0;
        if(add_new_point(t_list, e_list,  tri, p))
        {
            log_this("Failed to add point to triangulation\n");
            return 1;
        }
 
    }
/*  int f;
  for (f = 0; f < plist->points_used;f++)
    {
        p = plist->points + f;
  print_used_in(p);
    }*/
 
    if(constrained)
    {
        int res = restore_boundary(plist, e_list);
        if (res)
        {        
            log_this("Failed to restore boundary\n");
            return 1;
        }
    }

    return 0;
}


int cleanup(TRI_LIST *t_list)
{
    
    edge_list_destroy(t_list->e_list);
    plist_destroy(t_list->p_list);
    tri_list_destroy(t_list);
    return 0;
}

int triangulate_2_text(POINTLIST *plist, int constrained)
{
    
    int n_points = plist->tot_npoints;   
    
    TRI_LIST *t_list = tri_list_init((n_points + 3) * 2); //more than needed
    
    EDGE_LIST *e_list = edge_list_init((n_points + 3) * 3); //more than needed
    
    if(triangulate(plist, t_list, e_list,constrained))
    {
        cleanup(t_list);
        return 1;
    }
     
    print_all_triangels(t_list);
    cleanup(t_list);
    return 0;
    
}


TRI_LIST* triangulate_2_t_list(POINTLIST *plist, int constrained)
{
 //  log_this("lets start triangulation from postgis");
    int n_points = plist->tot_npoints;   
    
        
    TRI_LIST *t_list = tri_list_init((n_points + 3) * 2); //more than needed
    
    EDGE_LIST *e_list = edge_list_init((n_points + 3) * 3); //more than needed
    
    if(triangulate(plist, t_list, e_list,constrained))
    {
        cleanup(t_list);
        return NULL;
    }     
   
    return t_list;
    
}


